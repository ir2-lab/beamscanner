#include "scan.h"
#include <cmath>

scan::scan()
{

}

void scan::linear(int nx, int ny)
{
    nx += (nx & 1); // must be even
    x_.resize(nx*ny);
    y_.resize(nx*ny);
    bool d = true;
    double kx = 2./(nx-1);
    double ky = 2./(ny-1);
    int m = 0;

    // left to right
    for(int i=0; i<nx; i+=2) {
        for(int j=0; j<ny; j++) {
            x_[m] = -1. + kx*i;
            y_[m] = d ? 1. - ky*j : -1. + ky*j;
            m++;
        }
        d = !d;
    }

    // right to left
    for(int i=nx-1; i>0; i-=2) {
        for(int j=0; j<ny; j++) {
            x_[m] = -1. + kx*i;
            y_[m] = d ? 1. - ky*j : -1. + ky*j;
            m++;
        }
        d = !d;
    }
}

void scan::spiral(int nam, int nrot, int n)
{
    x_.resize(n);
    y_.resize(n);
    double kt = 1./n;
    double kw = 2*M_PI*nrot;
    for(int i=0; i<n; i++) {
        double t = kt*i;
        double w = triang(nam*t);
        w = (w>=0) ? sqrt(w) : -sqrt(-w);
        double wt = kw*t;
        x_[i] = w*sin(wt);
        y_[i] = w*cos(wt);
        if (t>0.5) x_[i] = -x_[i]; // ????
    }
}

void scan::lissajous(int nx, int ny, int n, double dphi)
{
    x_.resize(n);
    y_.resize(n);
    double kx = 2.*M_PI*nx/n;
    double ky = 2.*M_PI*ny/n;
    double dphi_ = dphi/180*M_PI;
    for(int i=0; i<n; i++) {
        x_[i] = sin(kx*i);
        y_[i] = sin(ky*i+dphi_);
    }
}

double scan::triang(const double& x)
{
    double y = 4.*(x - floor(x));
    if (y>=3.) y -= 4.;
    else if (y>=1.) y = 2.-y;
    return y;
}
