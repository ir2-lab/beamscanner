#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QVector>

#include "usb3105.h"

#include "scan.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void outErr(const QString& msg);

public slots:

    void on_btReadMe_clicked();

    // Scan options
    void on_btStart_toggled(bool b);

    void on_sbScanPeriod_valueChanged(double d)
    { ScanPeriod = d; updateScan(); }
    void on_sbAx_valueChanged(double d)
    { Ax = d; updateScan(); }
    void on_sbAy_valueChanged(double d)
    { Ay = d; updateScan(); }
    void on_sbVx_valueChanged(double d)
    { Vout[0] = d; updateScan(); }
    void on_sbVy_valueChanged(double d)
    { Vout[1] = d; updateScan(); }


    // Lissajous
    void on_sbNx_valueChanged(int d)
    { Nx = d; updateScan(); }
    void on_sbNy_valueChanged(int d)
    { Ny = d; updateScan(); }
    void on_sbPhase_valueChanged(int d)
    { Phase = d; updateScan(); }

    // Spiral
    void on_sbNam_valueChanged(int d)
    { Nam = d; updateScan(); }
    void on_sbNrot_valueChanged(int d)
    { Nrot = d; updateScan(); }

    // Linear
    void on_sbNl_valueChanged(int d)
    { Nl = d; updateScan(); }
    void on_rbVertical_toggled(bool b)
    { VerticalLines = b; updateScan(); }

    void on_tbPattern_currentChanged(int)
    { updateScan(); }

protected:
    void timerEvent(QTimerEvent *) override;
    void closeEvent(QCloseEvent* e) override;

private:
    Ui::Widget *ui;
    double Vout[2], Ax, Ay, Phase, ScanPeriod;
    int Nam, Nrot, Nx, Ny, Nl;
    bool VerticalLines;

    enum State {Disconnected, Connected, Scanning} state_;
    void setState(State s);

    void updateScan();

    usb3105* dev;
    scan scan_pattern;
    QVector<double> data, x, y, x0, y0;

    QDialog* dlgReadMe;
};
#endif // WIDGET_H
