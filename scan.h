#ifndef SCAN_H
#define SCAN_H

#include <QVector>

class scan
{
protected:
    QVector<double> x_, y_;
public:
    scan();
    const QVector<double>& x() const { return x_; }
    const QVector<double>& y() const { return y_; }
    int size() const { return x_.size(); }
    // nam_ : # of amplitude modulation cycles in the scan
    // nrot_: # of rotation cycles in the scan
    void spiral(int nam, int nrot, int n);
    // nx: # of x cycles
    // ny: # of y cycles
    void lissajous(int nx, int ny, int n, double dphi);
    void linear(int nx, int ny);
    // function returns a triangular waveform with period 1
    static double triang(const double& x);
    void swapxy() { x_.swap(y_); }
};

#endif // SCAN_H
