clear

[x,y] = rectscan(6,3,1.2,0.6);
N = length(x);
t = (0:N-1)';

FWHM = 0.1;

% x = x/max(abs(x));
% y = y/max(abs(y));


figure 1
clf

subplot(2,2,1)
plot(x,y,'.-')
xlim([-1 1])
ylim([-1 1])
axis equal

subplot(2,2,2)
plot(t,[x y],'.-')


sig = FWHM/(2*sqrt(2*log(2)));

L = 1;
M = 41;
range = linspace(-L,L,M);
[X,Y]=meshgrid(range,range);

Z = zeros(M,M);
for i=1:N,
  Z += exp(-0.5/sig*((X-x(i)).^2 + (Y-y(i)).^2))/N;
end

subplot(2,2,3)
surf(X,Y,Z)
colormap jet

subplot(2,2,4)
imagesc(range,range,Z,[0 max(max(Z))])
colormap jet
colorbar
