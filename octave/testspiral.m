clear

N = 200;
t = (0:N)';
[x,y,w] = spiralscan(3,5,N);

FWHM = 0.1;

% x = x/max(abs(x));
% y = y/max(abs(y));


figure 1
clf

subplot(2,2,1)
i1 = 1:round(N/4);
i2 = round(N/4):round(N/2);
i3 = round(N/2):round(3*N/4);
i4 = round(3*N/4):N+1;
plot(x(i1),y(i1),'.-')
hold on
plot(x(i2),y(i2),'.-')
plot(x(i3),y(i3),'.-')
plot(x(i4),y(i4),'.-')
hold off
xlim([-1 1])
ylim([-1 1])
axis equal

subplot(2,2,2)
plot(t,[x y],'.-')


sig = FWHM/(2*sqrt(2*log(2)));

L = 1;
M = 41;
range = linspace(-L,L,M);
[X,Y]=meshgrid(range,range);

Z = zeros(M,M);
for i=1:N,
  Z += exp(-0.5/sig*((X-x(i)).^2 + (Y-y(i)).^2))/N;
end

subplot(2,2,3)
surf(X,Y,Z)
colormap jet

subplot(2,2,4)
imagesc(range,range,Z,[0 max(max(Z))])
colormap jet
colorbar
