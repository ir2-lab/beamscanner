function [x,y] = rectscan(nx,ny,lx,ly)

  rx = linspace(-lx/2,lx/2,nx)';
  ry = linspace(-ly/2,ly/2,ny)';

  x = []; y = [];
  d = 1;
  for i=1:nx,
    x = [x; ones(ny,1)*rx(i)];
    if d,
      y = [y; ry];
    else
      y = [y; flipud(ry)];
    endif
    d = !d;
  endfor

end


