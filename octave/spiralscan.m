function [x,y,w] = spiralscan(FratioAM,FratioRot,N)
  x = zeros(N+1,1);
  y = zeros(N+1,1);
  t = (0:N)'/N;

  w = triang(FratioAM*t);
  i = find(w>=0);
  w(i) = sqrt(w(i));
  i = find(w<0);
  w(i) = -sqrt(abs(w(i)));
  x = w.*sin(2*pi*FratioRot*t);
  y = w.*cos(2*pi*FratioRot*t);

  i = find(t>=0.5);
  x(i) = - x(i);

end

function y = triang(x)
  x = 4*(x - floor(x));
  y = x;
  i = find(x>1 & x<=3);
  y(i) = 2-x(i);
  i = find(x>3);
  y(i) = x(i)-4;
end

