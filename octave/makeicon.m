clear

N = 1024;

f = 4;

N = 2^6;
F = 100. / N;
FratioAM = 1;
FratioRot = 4;
M = max(FratioAM,FratioRot)*N;

        if (FratioAM>=FratioRot) 
            Fam = F; Frot = FratioRot*Fam/FratioAM;
        else 
            Frot = F; Fam = FratioAM*Frot/FratioRot;
        end

        
        x = zeros(M+1,1);
        y = zeros(M+1,1);

        
        kr = 2*pi*Frot/100;
        kam = Fam/100;
        
        for i=1:M+1,
            s = sin(kr*(i-1));
            c = cos(kr*(i-1));
            w = kam*(i-1);
            w = 4*(w - floor(w));
            if (w<=1) w = sqrt(w);
            elseif (w<=2) w = sqrt(2 - w);
            elseif (w<=3) w =-sqrt(w - 2);
            else w = -sqrt(4-w);
            end
            
            x(i) = w*s;
            y(i) = w*c*0.9;
        end
    

    figure 1
    clf
    plot(x,y,'linewidth',3)
    xlim([-1 1])
    ylim([-1 1])
set(gca,'position',[0.02 0.02 0.98 0.98])
axis equal
axis off
print -dsvg beamscanner
print -dpng -S"128,128" beamscanner128
print -dpng -S"64,64" beamscanner64