#!/bin/bash
#
# 
# Submodule stuff from Khaja Minhajuddin
# File name: makeArchive.sh
# cd qdaq-root-folder; ./makeArchive.sh

prefix="beamscanner-0.5/"
filename="beamscanner-0.5"

ROOT_ARCHIVE_DIR="$(mktemp -d)"

echo "> create root archive"
git archive --format=tar --prefix=$prefix --output $ROOT_ARCHIVE_DIR"/repo-output.tar" HEAD 

echo "> append submodule archives"

pushd qmatplotwidget
git archive --format=tar --prefix=$prefix"qmatplotwidget/" --output $ROOT_ARCHIVE_DIR"/repo-output-sub1.tar" HEAD
popd
pushd $ROOT_ARCHIVE_DIR
tar --concatenate --file repo-output.tar repo-output-sub1.tar
popd

pushd $ROOT_ARCHIVE_DIR
echo "> remove all sub tars"
rm -rf repo-output-sub*.tar

echo "> gzip final tar"
gzip --force --verbose repo-output.tar
popd

echo "> move output file"
mv $ROOT_ARCHIVE_DIR"/repo-output.tar.gz" $filename".tar.gz"

echo "> done"
