Summary: Beam Scanner.
Name: beamscanner
Version: 0.3
Release: 1%{?dist}
License: MIT
Source0: %{name}-%{version}.tar.gz

Requires: qt5-qtbase
Requires: qt5-qtbase-gui
Requires: qwt-qt5
Requires: libuldaq

BuildRequires: cmake3
BuildRequires: qt5-qtbase-devel
BuildRequires: qwt-qt5-devel
BuildRequires: libuldaq

%description
Generate a beam scan pattern oon a MCC USB-3105 device.

%prep
%setup -q

%build
%{cmake3}
%{cmake3_build}

%install
%{cmake3_install}

%clean
rm -rf %{buildroot}

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files
%defattr(-,root,root)
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/128x128/apps/%{name}.png
%{_datadir}/icons/hicolor/64x64/apps/%{name}.png
%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg

%changelog
* Mon Aug 28 2023 George
- ver 0.1
- initial version of spec file

