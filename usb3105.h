#ifndef USB3105_H
#define USB3105_H

#include "uldaq.h"
#include <QString>
#include <QVector>
#include <QObject>
#include <QElapsedTimer>

#include "qtimerthread.h"

template<class T, int N>
class running_average
{
	T data[N];
	T* p,*pend;
public:
	explicit running_average(const T& v = 0)
	{
		p = &data[0];
		pend = p + N;
		while (p!=pend) *p++ = v;
		p = &data[0];
	}
	running_average& operator<<(const T& v)
	{
		*p++ = v;
		if (p==pend) p = &data[0];
		return *this;
	}
    T operator()() const
	{
		T m = 0;
		for(int i = 0; i<N; ++i) m += data[i];
		return m/N;
	}
	void clear()
	{
		for(int i=0; i<N; ++i) data[i]=0;
	}
};

#define MAX_DEV_COUNT  100

struct uldaq
{
private:
    
    unsigned int numDevs;
    DaqDeviceDescriptor devDescriptors[MAX_DEV_COUNT];
public:
    uldaq() : numDevs(0) {};
    static QString getErrMsg(int errCode);
    int enumerateDevices();
    QString infoStr(int i) const;
    QString devString(int i) const;
    const DaqDeviceDescriptor* devDescriptor(int i) const
    { return devDescriptors+i; }

};


class usb3105 : public QObject
{
    Q_OBJECT

    QString errmsg_;

    DaqDeviceHandle hdl_;

    // the timer thread
    class daqthread : public QTimerThread
    {
    protected:
        virtual void timer_func() { thisDev->updateOutput(); }
    public:
        usb3105* thisDev;
    };

    friend class daqthread;

    daqthread thread_;

    // thread variables
    int idx_;
    int len_;
    Range ranges_[2];
    QVector<double> data_;
    bool aborted_;

    // for thread loop timing
    QElapsedTimer clock_;
    running_average<float,10> perfmon_;
    float t_[2];

    // thread function
    void updateOutput();
    

public:
    usb3105(QObject* p = NULL);
    ~usb3105();

    int open(const DaqDeviceDescriptor& d);
    void close();
    bool isOpen();
    bool isRunning() const { return thread_.isRunning(); }
    float loopPeriod() const
    { return isRunning() ? perfmon_() : 0.f; }
    QString getInfo();


    QString getErrMsg() const { return errmsg_; }

    int start(uint msPerSample, const QVector<double>& data);
    void stop();
    int setOutput(const double* p);

protected slots:
    void onAbort();

signals:
    /// This is emitted if the loop aborts due to an error.
    void abort();
};

#endif // USB3105_H
