#include "usb3105.h"



#define MAX_DEV_COUNT  100
#define MAX_STR_LENGTH 64

void ConvertRangeToMinMax(Range range, double* min, double* max)
{
	switch(range)
	{
	case(BIP60VOLTS):
		*min = -60.0;
		*max = 60.0;
		break;
	case(BIP30VOLTS):
		*min = -30.0;
		*max = 30.0;
		break;
	case(BIP15VOLTS):
		*min = -15.0;
		*max = 15.0;
		break;
	case(BIP20VOLTS):
		*min = -20.0;
		*max = 20.0;
		break;
	case(BIP10VOLTS):
		*min = -10.0;
		*max = 10.0;
		break;
	case(BIP5VOLTS):
		*min = -5.0;
		*max = 5.0;
		break;
	case(BIP4VOLTS):
		*min = -4.0;
		*max = 4.0;
		break;
	case(BIP2PT5VOLTS):
		*min = -2.5;
		*max = 2.5;
		break;
	case(BIP2VOLTS):
		*min = -2.0;
		*max = 2.0;
		break;
	case(BIP1PT25VOLTS):
		*min = -1.25;
		*max = 1.25;
		break;
	case(BIP1VOLTS):
		*min = -1.0;
		*max = 1.0;
		break;
	case(BIPPT625VOLTS):
		*min = -0.625;
		*max = 0.625;
		break;
	case(BIPPT5VOLTS):
		*min = -0.5;
		*max = 0.5;
		break;
	case(BIPPT25VOLTS):
		*min = -0.25;
		*max = 0.25;
		break;
	case(BIPPT125VOLTS):
		*min = -0.125;
		*max = 0.125;
		break;
	case(BIPPT2VOLTS):
		*min = -0.2;
		*max = 0.2;
		break;
	case(BIPPT1VOLTS):
		*min = -0.1;
		*max = 0.1;
		break;
	case(BIPPT078VOLTS):
		*min = -0.078;
		*max = 0.078;
		break;
	case(BIPPT05VOLTS):
		*min = -0.05;
		*max = 0.05;
		break;
	case(BIPPT01VOLTS):
		*min = -0.01;
		*max = 0.01;
		break;
	case(BIPPT005VOLTS):
		*min = -0.005;
		*max = 0.005;
		break;
	case(BIP3VOLTS):
		*min = -3.0;
		*max = 3.0;
		break;
	case(BIPPT312VOLTS):
		*min = -0.312;
		*max = 0.312;
		break;
	case(BIPPT156VOLTS):
		*min = -0.156;
		*max = 0.156;
		break;

	case(UNI60VOLTS):
		*min = 0.0;
		*max = 60.0;
		break;
	case(UNI30VOLTS):
		*min = 0.0;
		*max = 30.0;
		break;
	case(UNI15VOLTS):
		*min = 0.0;
		*max = 15.0;
		break;
	case(UNI20VOLTS):
		*min = 0.0;
		*max = 20.0;
		break;
	case(UNI10VOLTS):
		*min = 0.0;
		*max = 10.0;
		break;
	case(UNI5VOLTS):
		*min = 0.0;
		*max = 5.0;
		break;
	case(UNI4VOLTS):
		*min = 0.0;
		*max = 4.0;
		break;
	case(UNI2PT5VOLTS):
		*min = 0.0;
		*max = 2.5;
		break;
	case(UNI2VOLTS):
		*min = 0.0;
		*max = 2.0;
		break;
	case(UNI1PT25VOLTS):
		*min = 0.0;
		*max = 1.25;
		break;
	case(UNI1VOLTS):
		*min = 0.0;
		*max = 1.0;
		break;
	case(UNIPT625VOLTS):
		*min = 0.0;
		*max = 0.625;
		break;
	case(UNIPT5VOLTS):
		*min = 0.0;
		*max = 0.5;
		break;
	case(UNIPT25VOLTS):
		*min = 0.0;
		*max = 0.25;
		break;
	case(UNIPT125VOLTS):
		*min = 0.0;
		*max = 0.125;
		break;
	case(UNIPT2VOLTS):
		*min = 0.0;
		*max = 0.2;
		break;
	case(UNIPT1VOLTS):
		*min = 0.0;
		*max = 0.1;
		break;
	case(UNIPT078VOLTS):
		*min = 0.0;
		*max = 0.078;
		break;
	case(UNIPT05VOLTS):
		*min = 0.0;
		*max = 0.05;
		break;
	case(UNIPT01VOLTS):
		*min = 0.0;
		*max = 0.1;
		break;
	case(UNIPT005VOLTS):
		*min = 0.0;
		*max = 0.005;
		break;
	case(MA0TO20):
		*min = 0.0;
		*max = 20.0;
		break;
	}
}

QString uldaq::getErrMsg(int errCode) {
    char errMsg[ERR_MSG_LEN];
    UlError err = ulGetErrMsg((UlError)errCode, errMsg);
    if (err != ERR_NO_ERROR)
        return QString(errMsg);
    else return QString("Unknown error code %1").arg(errCode);
}

int uldaq::enumerateDevices() {
    DaqDeviceInterface interfaceType = ANY_IFC;
	numDevs = MAX_DEV_COUNT; 
    UlError err =  ulGetDaqDeviceInventory(interfaceType, devDescriptors, &numDevs);
    return (err == ERR_NO_ERROR) ? numDevs : -1;
}

QString uldaq::infoStr(int i) const {
    if (i < numDevs) {
        return QString("%1: (%2)").arg(devDescriptors[i].productName).arg(devDescriptors[i].uniqueId);
    }
    else return QString();
}

QString uldaq::devString(int i) const {
    if (i < numDevs) return QString(devDescriptors[i].devString);
    else return QString();
}

usb3105::usb3105(QObject* p) : QObject(p), hdl_(0)
{
    thread_.thisDev = this;
    ranges_[0] = BIP10VOLTS;
    ranges_[1] = BIP10VOLTS;
    connect(this,SIGNAL(abort()),this,SLOT(onAbort()),Qt::QueuedConnection);
}

usb3105::~usb3105() {
    close();
}

int usb3105::open(const DaqDeviceDescriptor& d)
{
    if (isOpen()) return hdl_;

    hdl_ = ulCreateDaqDevice(d);

    // establish a connection to the device
    UlError err = ulConnectDaqDevice(hdl_);
    if (err != ERR_NO_ERROR) {
        errmsg_ = uldaq::getErrMsg(err);
        ulReleaseDaqDevice(hdl_);
        hdl_ = 0;
    }

    return hdl_;
}

QString usb3105::getInfo()
{
    QString msg;
    if (!isOpen()) return msg;
    UlError err = ERR_NO_ERROR;
    long long infoValue;
    if ((err=ulAOGetInfo(hdl_, AO_INFO_RESOLUTION, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg = QString("Resolution (bits): %1").arg(infoValue);
    if ((err = ulAOGetInfo(hdl_, AO_INFO_NUM_CHANS, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", Channels: %1").arg(infoValue);
    if ((err = ulAOGetInfo(hdl_, AO_INFO_SCAN_OPTIONS, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", Scan options: %1").arg(infoValue);
    if ((err = ulAOGetInfo(hdl_, AO_INFO_HAS_PACER, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", Pacer: %1").arg(infoValue);
    if ((err = ulAOGetInfo(hdl_, AO_INFO_NUM_RANGES, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", # of Ranges: %1").arg(infoValue);
    unsigned int nRanges = (int)infoValue;
    for (unsigned int i=0; i<nRanges; i++) {
        if ((err = ulAOGetInfo(hdl_, AO_INFO_RANGE, i, &infoValue)) != ERR_NO_ERROR)
            return msg;
        double vmin, vmax;
        ConvertRangeToMinMax((Range)infoValue,&vmin,&vmax);
        msg += QString(", %1: %2...%3").arg(i).arg(vmin).arg(vmax);
    }
    if ((err = ulAOGetInfo(hdl_, AO_INFO_TRIG_TYPES, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", Trigger Types: %1").arg(infoValue);
    if ((err = ulAOGetInfo(hdl_, AO_INFO_FIFO_SIZE, 0, &infoValue)) != ERR_NO_ERROR)
        return msg;
    msg += QString(", FIFO size: %1").arg(infoValue);
    return msg;
}

int usb3105::start(uint msPerSample, const QVector<double>& data)
{   
    if (!isOpen()) return 0;

    thread_.setInterval(msPerSample);
    idx_ = 0;
    len_ = data.size();
    data_ = data;
    t_[0] = t_[1] = 0;
    aborted_ = false;
    clock_.start();
    thread_.start();

    return 1;
}

void usb3105::stop()
{
    if (!thread_.isRunning()) return;
    thread_.quit();
    thread_.wait();
}

void usb3105::updateOutput()
{
    if (aborted_) return;

    // check time for loop statistics
    t_[1] = 1e-6f*clock_.nsecsElapsed();

    // write data to USB-3105
    const double* p = data_.constData() + idx_;
    UlError err = ulAOutArray(hdl_,0,1,ranges_,
      AOUTARRAY_FF_DEFAULT,
      (double*)p);
    if (err != ERR_NO_ERROR) {
        aborted_ = true;
        errmsg_ = uldaq::getErrMsg(err);
        emit abort();
    }
    idx_ += 2;
    idx_ %= len_;

    // loop statistics
    perfmon_ << (t_[1] - t_[0]); t_[0] = t_[1];

}

int usb3105::setOutput(const double* p)
{
    if (!isOpen()) return 0;
    if (isRunning()) return 0;

    UlError err = ulAOutArray(hdl_,0,1,ranges_,
      AOUTARRAY_FF_DEFAULT,
      (double*)p);
    if (err != ERR_NO_ERROR) {
        errmsg_ = uldaq::getErrMsg(err);
        return 0;
    }
    return 1;
}

void usb3105::onAbort()
{
    stop();
    isOpen();
}

bool usb3105::isOpen()
{
    if (hdl_) {
        if (thread_.isRunning()) return true;
        else {
            int ret = 0;
            UlError err = ulIsDaqDeviceConnected(hdl_,&ret);
            if (err==ERR_NO_ERROR) {
                if (ret) return true;
                else {
                    ulReleaseDaqDevice(hdl_);
                    hdl_ = 0;
                    return false;
                }
            } else {
                errmsg_ = uldaq::getErrMsg(err);
                ulReleaseDaqDevice(hdl_);
                hdl_ = 0;
                return false;
            }
        }
    } else return false;
}

void usb3105::close()
{
    if (hdl_) {
        if (thread_.isRunning()) stop();
        UlError err = ulDisconnectDaqDevice(hdl_);
        err = ulReleaseDaqDevice(hdl_);
        hdl_ = 0;
    }
}
