# BeamScanner v0.5

This program connects to a USB-3105 DAC device and 
creates a X-Y scan pattern for uniform irradiation of 
an area by the ion beam. 

The output from USB-3105 can
drive a set of power supplies connected to magnetic or
electrostatic beam deflectors.

Project repo: https://gitlab.com/ir2-lab/beamscanner

## Usage

- Connect the USB-3105 to a usb port. The status in the BeamScanner window should change to "Connected"
- Set the general scan parameters: scan period, amplitude and bias
- Select the scan mode (Spiral, Linear or Lissajous) and adjust the relevant parameters
- Observe the scan on the graph pane.
- Press ``Start/Stop`` to start the scan

**Note**: With the USB device connected, the bias voltage is continuously updated at the device terminals.

## Scan modes
- **Spiral:** Generates a wobbling scan around the center which ensures uniform irradiation of a circular area. It consists of a beam rotation where the amplitude is modulated proportionally to sqrt(t).
- **Linear:** Generates a linear pattern with either vertical or horizontal lines
- **Lissajous:** Generates a Lissajous figure for testing purposes (this is not a uniform irradiation scan) 

In all cases the output is updated every 10ms (100Hz base frequency). The scan period Tsc defines the number of points N in the scan as N = 100*Tsc. The user must check in the graph that the point density is sufficient to generate a relatively smooth scanning operation. If this is not the case then Tsc should be increased.

## Electrical specifications

The X and Y scan components are sent to USB-3105 channels 0 and 1, respectively.
The output is bipolar to max. ±10V.

