# beamscanner

A program to generate X-Y beam scan patterns with the USB-3105 DAC device.

![Screenshot](images/beamscanner_screenshot.png)

