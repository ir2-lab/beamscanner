#include "widget.h"

#include "ui_widget.h"
#include "ui_help.h"

#include <QMessageBox>
#include <QCloseEvent>

#include <cmath>

class ReadMe : public QDialog
{
    Ui::Dialog* ui;
public:
    ReadMe(QWidget* p = 0) : QDialog(p),
        ui(new Ui::Dialog)
    {
        ui->setupUi(this);
    }
    ~ReadMe()
    {
        delete ui;
    }

};

Widget::Widget(QWidget *parent)
    : QWidget(parent),
      ui(new Ui::Widget),
      Ax(10.), Ay(10.),
      ScanPeriod(1.),
      Nam(1), Nrot(4), Nx(1), Ny(2), Phase(0.),
      Nl(4), VerticalLines(true),
      state_(Scanning),
      dlgReadMe(0)
{
    ui->setupUi(this);

    dev = new usb3105(this);

    setWindowTitle("Beam Scanner");

    Vout[0] = Vout[1] = 0;

    x0.resize(1);
    y0.resize(1);
    x0[0] = 0.; y0[0]=0.;

    setState(Disconnected);

    updateScan();

    startTimer(500); // status timer
}

Widget::~Widget()
{
    delete ui;
}

void Widget::closeEvent(QCloseEvent * e)
{
    if (state_==Scanning) {
        int ret = QMessageBox::critical(this,
                                        "BeamScanner",
                                        "Scan operation is active.\n"
                                        "Are you sure you want to quit?",
                                        QMessageBox::Ok | QMessageBox::Cancel);
        if (ret != QMessageBox::Ok) {
            e->ignore();
            return;
        }

    }
    e->accept();
    if (dlgReadMe) {
        dlgReadMe->close();
        dlgReadMe->deleteLater();
    }
}

void Widget::on_btReadMe_clicked()
{
    if (dlgReadMe==0) dlgReadMe = new ReadMe;
    dlgReadMe->show();
}

void Widget::outErr(const QString& msg)
{
    //ui->edtOutput->	appendPlainText(msg);
    //ui->edtOutput->	appendPlainText("\n");
    ui->edtStatus->setText(msg);
}

void Widget::on_btStart_toggled(bool b)
{
    if (b) {
        dev->start(10,data);
        setState(Scanning);
    } else {
        dev->stop();
        setState(Connected);
        dev->setOutput(Vout);
    }
}

void Widget::updateScan()
{
    int tbi = ui->tbPattern->currentIndex();
    QString Pattern = ui->tbPattern->tabText(tbi);

    int Npts = (int)ceil(ScanPeriod*100);

    if (Pattern=="Lissajous")
        scan_pattern.lissajous(Nx,Ny,Npts,Phase);
    else if (Pattern=="Spiral")
        scan_pattern.spiral(Nam,Nrot,Npts);
    else { // linear
        int nx = 2*Nl;
        int ny = (int)ceil(1.*Npts/nx);
        scan_pattern.linear(nx,ny);
        Npts = nx*ny;
        if (!VerticalLines) scan_pattern.swapxy();
    }


    data.resize(2*Npts);
    x.resize(Npts);
    y.resize(Npts);
    const QVector<double>& sx = scan_pattern.x();
    const QVector<double>& sy = scan_pattern.y();
    double *p = data.data();
    for(int i=0; i<Npts; i++) {
        double v = Ax*sx[i] + Vout[0];
        if (v>10.) v = 10.;
        else if (v<-10.) v = -10.;
        *p++ = x[i] = v;

        v = Ay*sy[i] + Vout[1];
        if (v>10.) v = 10.;
        else if (v<-10.) v = -10.;
        *p++ = y[i] = v;
    }
    x0[0] = Vout[0];
    y0[0] = Vout[1];

    ui->wPlot->clear();
    ui->wPlot->plot(x,y,".-");
    ui->wPlot->plot(x0,y0,"+");
    ui->wPlot->setXlim(QPointF(-10.,10.));
    ui->wPlot->setYlim(QPointF(-10.,10.));
    ui->wPlot->setGrid(true);

    if (state_==Connected) dev->setOutput(Vout);
}

void Widget::setState(State s)
{
    if (s==state_) return;   
    switch (s) {
    case Disconnected:
        outErr("Disconnected");
        ui->lblDevIcon->setEnabled(false);
        ui->btStart->setEnabled(false);
        ui->btStart->setChecked(false);
        ui->gbPattern->setEnabled(true);
        updateScan();
        break;
    case Connected:
        outErr("Connected");
        ui->lblDevIcon->setEnabled(true);
        ui->btStart->setEnabled(true);
        ui->btStart->setChecked(false);
        ui->gbPattern->setEnabled(true);
        updateScan();
        break;
    case Scanning:
        double d = 1.*dev->loopPeriod();
        outErr(QString("Scanning - %1ms/pt").arg(d,4,'f',1));
        ui->lblDevIcon->setEnabled(true);
        ui->gbPattern->setEnabled(false);
        break;
    }

    state_ = s;
}

void Widget::timerEvent(QTimerEvent *)
{
    if (dev->isOpen()) {
        if (dev->isRunning()) {
            double d = 1.*dev->loopPeriod();
            outErr(QString("Scanning - %1ms/pt").arg(d,4,'f',1));
            setState(Scanning);
        } else {
            setState(Connected);
        }
    } else {
        uldaq ud;
        int ret = ud.enumerateDevices();
        if (ret<=0) setState(Disconnected);
        else {
            int i = 0;
            for(; i<ret; i++)
                if (ud.devString(i) == "USB-3105") break;

            if (i<ret) {

                if (dev->open(*(ud.devDescriptor(i))))
                    setState(Connected);
                else {
                    dev->close();
                    setState(Disconnected);
                }
            }
            else setState(Disconnected);
        }
    }
}





